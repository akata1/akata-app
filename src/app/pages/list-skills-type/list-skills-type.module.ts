import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListSkillsTypePageRoutingModule } from './list-skills-type-routing.module';

import { ListSkillsTypePage } from './list-skills-type.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListSkillsTypePageRoutingModule
  ],
  declarations: [ListSkillsTypePage]
})
export class ListSkillsTypePageModule {}
