import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListSkillsTypePage } from './list-skills-type.page';

const routes: Routes = [
  {
    path: '',
    component: ListSkillsTypePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListSkillsTypePageRoutingModule {}
