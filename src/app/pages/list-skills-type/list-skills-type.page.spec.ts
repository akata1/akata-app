import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListSkillsTypePage } from './list-skills-type.page';

describe('ListSkillsTypePage', () => {
  let component: ListSkillsTypePage;
  let fixture: ComponentFixture<ListSkillsTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSkillsTypePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListSkillsTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
