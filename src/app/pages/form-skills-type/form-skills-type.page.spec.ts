import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormSkillsTypePage } from './form-skills-type.page';

describe('FormSkillsTypePage', () => {
  let component: FormSkillsTypePage;
  let fixture: ComponentFixture<FormSkillsTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSkillsTypePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormSkillsTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
