import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormSkillsTypePage } from './form-skills-type.page';

const routes: Routes = [
  {
    path: '',
    component: FormSkillsTypePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormSkillsTypePageRoutingModule {}
