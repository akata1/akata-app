import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormSkillsTypePageRoutingModule } from './form-skills-type-routing.module';

import { FormSkillsTypePage } from './form-skills-type.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormSkillsTypePageRoutingModule
  ],
  declarations: [FormSkillsTypePage]
})
export class FormSkillsTypePageModule {}
