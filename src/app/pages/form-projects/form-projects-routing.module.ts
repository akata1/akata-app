import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormProjectsPage } from './form-projects.page';

const routes: Routes = [
  {
    path: '',
    component: FormProjectsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormProjectsPageRoutingModule {}
