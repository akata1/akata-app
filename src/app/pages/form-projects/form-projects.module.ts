import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormProjectsPageRoutingModule } from './form-projects-routing.module';

import { FormProjectsPage } from './form-projects.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormProjectsPageRoutingModule
  ],
  declarations: [FormProjectsPage]
})
export class FormProjectsPageModule {}
