import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormProjectsPage } from './form-projects.page';

describe('FormProjectsPage', () => {
  let component: FormProjectsPage;
  let fixture: ComponentFixture<FormProjectsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormProjectsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormProjectsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
