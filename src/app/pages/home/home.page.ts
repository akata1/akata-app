import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, MenuController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  constructor(
    private menuCtrl: MenuController,
    private actionSheetController: ActionSheetController,
    private authService: AuthService,
    private router : Router
  ) {}

  public async openMenu() {
    await this.menuCtrl.open();
  }

  public async goToUsers() {
    this.router.navigate(["home"]);
  }
  public async goToMembers() {
    this.router.navigate(["list-user"]);
  }
  public async goToSkills() {
    this.router.navigate(["list-skills"]);
  }
  public async goToSkillsType() {
    this.router.navigate(["list-skills-type"]);
  }
  public async goToProjects() {
    this.router.navigate(["list-projects"]);
  }
  public async goToGroups() {
    this.router.navigate(["list-group"]);
  }

  public async logOut() {
    this.authService.SignOut();
  }

  async actionSheetButton() {
    const actionSheet = await this.actionSheetController.create({
      header: "Albums",
      cssClass: "my-custom-class",
      buttons: [
        {
          text: "View photo",
          icon: "eye",
          handler: () => {
            console.log("Play clicked");
          },
        },
        {
          text: "Edit",
          icon: "create",
          handler: () => {
            console.log("Share clicked");
          },
        },
        {
          text: "Delete",
          role: "destructive",
          icon: "trash",
          handler: () => {
            console.log("Delete clicked");
          },
        },
        {
          text: "Cancel",
          icon: "close",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          },
        },
      ],
    });
    await actionSheet.present();
  }
}
