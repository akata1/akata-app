import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormSkillsPage } from './form-skills.page';

describe('FormSkillsPage', () => {
  let component: FormSkillsPage;
  let fixture: ComponentFixture<FormSkillsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSkillsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormSkillsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
