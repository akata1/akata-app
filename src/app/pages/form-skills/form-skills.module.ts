import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormSkillsPageRoutingModule } from './form-skills-routing.module';

import { FormSkillsPage } from './form-skills.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormSkillsPageRoutingModule
  ],
  declarations: [FormSkillsPage]
})
export class FormSkillsPageModule {}
