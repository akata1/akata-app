import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormSkillsPage } from './form-skills.page';

const routes: Routes = [
  {
    path: '',
    component: FormSkillsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormSkillsPageRoutingModule {}
