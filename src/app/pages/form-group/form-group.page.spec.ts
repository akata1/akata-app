import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormGroupPage } from './form-group.page';

describe('FormGroupPage', () => {
  let component: FormGroupPage;
  let fixture: ComponentFixture<FormGroupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormGroupPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormGroupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
