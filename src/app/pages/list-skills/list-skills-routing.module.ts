import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListSkillsPage } from './list-skills.page';

const routes: Routes = [
  {
    path: '',
    component: ListSkillsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListSkillsPageRoutingModule {}
