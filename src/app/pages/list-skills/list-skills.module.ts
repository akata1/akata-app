import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListSkillsPageRoutingModule } from './list-skills-routing.module';

import { ListSkillsPage } from './list-skills.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListSkillsPageRoutingModule
  ],
  declarations: [ListSkillsPage]
})
export class ListSkillsPageModule {}
