import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListSkillsPage } from './list-skills.page';

describe('ListSkillsPage', () => {
  let component: ListSkillsPage;
  let fixture: ComponentFixture<ListSkillsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSkillsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListSkillsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
