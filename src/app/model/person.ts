import { Contact } from './contact';
import { Skill } from './skill';

export type Person = {
    id?: string,
    name?: string,
    firstName?: string,
    age?: number,
    title?: string,
    photo?: string,
    hourlyPrice?: string,
    aboutMe?: string,
    welcomePhrase?: string,
    openTime?: string,
    closeTime?: string,
    callToAction?: string,
    contact?: Contact,
    skill?: Skill
}