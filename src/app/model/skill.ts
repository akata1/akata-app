import { TypeSkill } from './typeSkill';

export type Skill = {
    id?: string,
    title?: string,
    description?: string,
    image?: string,
    type?: TypeSkill
    
}