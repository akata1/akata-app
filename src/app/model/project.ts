import { Person } from './person';

export type Project = {
    id?: string,
    title?: string,
    image?: string,
    person?: Person
}