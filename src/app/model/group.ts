import { Project } from './project';
import { Skill } from './skill';

export type Group = {
    id?: string,
    name?: string,
    image?: string,
    skill?: Skill,
    project?: Project
}