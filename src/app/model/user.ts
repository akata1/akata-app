export type User = {
  uid: string;
  email: string;
  pseudo: string;
  photoURL: string;
}
