export type Contact = {
    id?: string,
    libelle?: string,
    value?: string,
    link?: string
}