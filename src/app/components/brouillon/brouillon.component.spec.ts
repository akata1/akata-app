import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BrouillonComponent } from './brouillon.component';

describe('BrouillonComponent', () => {
  let component: BrouillonComponent;
  let fixture: ComponentFixture<BrouillonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrouillonComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BrouillonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
