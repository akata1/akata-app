import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'list-user',
    loadChildren: () => import('./pages/list-user/list-user.module').then( m => m.ListUserPageModule)
  },
  {
    path: 'form-user',
    loadChildren: () => import('./pages/form-user/form-user.module').then( m => m.FormUserPageModule)
  },
  {
    path: 'list-skills',
    loadChildren: () => import('./pages/list-skills/list-skills.module').then( m => m.ListSkillsPageModule)
  },
  {
    path: 'form-skills',
    loadChildren: () => import('./pages/form-skills/form-skills.module').then( m => m.FormSkillsPageModule)
  },
  {
    path: 'list-skills-type',
    loadChildren: () => import('./pages/list-skills-type/list-skills-type.module').then( m => m.ListSkillsTypePageModule)
  },
  {
    path: 'form-skills-type',
    loadChildren: () => import('./pages/form-skills-type/form-skills-type.module').then( m => m.FormSkillsTypePageModule)
  },
  {
    path: 'list-projects',
    loadChildren: () => import('./pages/list-projects/list-projects.module').then( m => m.ListProjectsPageModule)
  },
  {
    path: 'form-projects',
    loadChildren: () => import('./pages/form-projects/form-projects.module').then( m => m.FormProjectsPageModule)
  },
  {
    path: 'list-group',
    loadChildren: () => import('./pages/list-group/list-group.module').then( m => m.ListGroupPageModule)
  },
  {
    path: 'form-group',
    loadChildren: () => import('./pages/form-group/form-group.module').then( m => m.FormGroupPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
