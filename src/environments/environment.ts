// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyAR-y9hYVsRhPai5r3h_OMa6RbTUdmq2Is",
    authDomain: "ionictraining-64234.firebaseapp.com",
    databaseURL: "https://ionictraining-64234.firebaseio.com",
    projectId: "ionictraining-64234",
    storageBucket: "ionictraining-64234.appspot.com",
    messagingSenderId: "76570041140",
    appId: "1:76570041140:web:427794da9c9dbb3caf2361",
    measurementId: "G-8HLV8HE13J"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
